module.exports = {
 platform: 'gitlab',
 endpoint: 'https://gitlab.com/api/v4/',
 gitAuthor: "Renovate Bot <bot@renovateapp.com>",
 assignees: ['techisfun'],
 baseBranches: ['develop'],
 logLevel: 'info',
 labels: ['renovate'],
 extends: ['config:base'],
 repositories: ['techisfun/reference'],
 "gradle": {
    "enabled": false
  },
  "gradle-lite": {
    "enabled": true
  }
};
